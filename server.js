const http = require('http');
const path = require('path');
const uuid = require('uuid');
const url = require('url');
const fs = require('fs');

const app = http.createServer((req, res) => {


        if (req.url === '/GET/html') {
            res.writeHead(200, { 'Content-Type' : 'text/html'});

            const filePath = path.join(__dirname, 'public', 'index.html');
            

            fs.readFile(filePath, (err, data) => {
                if (err) {                   
                    res.writeHead(500);
                    res.end('Error reading HTML file');
                } else {
                    res.end(data);
                }            
            });
        } else if (req.url === '/GET/json') {

            const filePath = path.join(__dirname, 'public', 'data.json');

            fs.readFile(filePath, 'utf-8', (err, data) => {
                if (err) {
                    res.writeHead(500);
                    res.end('Error reading Json file');
                } else {
                    res.end(data);
                }            
            });
        } else if (req.url === '/GET/uuid' ){

            let uuidElement = uuid.v4();
            let uuidObject = {uuid:uuidElement}
            res.end(JSON.stringify(uuidObject));
        }

        const parseUrl = url.parse(req.url);
        const urlPathName = parseUrl.pathname;
        const codeArray = urlPathName.split('/');
        const code = codeArray[codeArray.length - 1];
        const delay = code;

        if (req.url === '/GET/status/'+code ){
            res.end(http.STATUS_CODES[code])
        } 

        if (req.url === '/GET/delay/'+ delay){
            setTimeout(()=>{
                res.end(http.STATUS_CODES[200]);
            }, delay*1000);
        }
});

app.listen(4000, () => {
    console.log(`Server listening on port ${4000}`);
});



